#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "database.h"

Persona* createPersona(char* name, char* surname, char* address, int age);
IndexNodeString* createNodeString(char* value);
IndexNodeInt* createNodeInt(int value);
void insertString(IndexNodeString *node, char* value);
void insertInt(IndexNodeInt *node, int value);

Persona* findByName_recursive(IndexNodeString *name_node, IndexNodeString *surname_node, IndexNodeString *address_node, IndexNodeInt *age_node, char* name);
Persona* findBySurname_recursive(IndexNodeString *name_node, IndexNodeString *surname_node, IndexNodeString *address_node, IndexNodeInt *age_node, char * surname);
Persona* findByAddress_recursive(IndexNodeString *name_node, IndexNodeString *surname_node, IndexNodeString *address_node, IndexNodeInt *age_node, char * address);
Persona* findByAge_recursive(IndexNodeString *name_node, IndexNodeString *surname_node, IndexNodeString *address_node, IndexNodeInt *age_node, int age);

Persona* findBySurname_recursive(IndexNodeString *name_node, IndexNodeString *surname_node, IndexNodeString *address_node, IndexNodeInt *age_node, char * surname){
	if(surname_node == NULL ) {
		return NULL;
	}
	if(strcmp(surname_node->value,surname)==0){
		Persona *out = createPersona(name_node->value,surname_node->value,address_node->value,age_node->value);
		printf("%s %s %s %d\n",name_node->value, surname_node->value, address_node->value, age_node->value);
		return out;
	}
	findBySurname_recursive(name_node->left, surname_node->left, address_node->left, age_node->left,surname);
	findBySurname_recursive(name_node->right, surname_node->right, address_node->right, age_node->right,surname);
}

Persona* findByAddress_recursive(IndexNodeString *name_node, IndexNodeString *surname_node, IndexNodeString *address_node, IndexNodeInt *age_node, char * address){
	if(address_node == NULL ) {
		return NULL;
	}
	if(strcmp(address_node->value,address)==0){
		Persona *out = createPersona(name_node->value,surname_node->value,address_node->value,age_node->value);
		printf("%s %s %s %d\n",name_node->value, surname_node->value, address_node->value, age_node->value);
		return out;
	}
	findByAddress_recursive(name_node->left, surname_node->left, address_node->left, age_node->left,address);
	findByAddress_recursive(name_node->right, surname_node->right, address_node->right, age_node->right,address);
}

Persona* findByAge_recursive(IndexNodeString *name_node, IndexNodeString *surname_node, IndexNodeString *address_node, IndexNodeInt *age_node, int age){
	if(age_node == NULL ) {
		return NULL;
	}
	if(age_node->value==age){
		Persona *out = createPersona(name_node->value,surname_node->value,address_node->value,age_node->value);
		printf("%s %s %s %d\n",name_node->value, surname_node->value, address_node->value, age_node->value);
		return out;
	}
	findByAge_recursive(name_node->left, surname_node->left, address_node->left, age_node->left,age);
	findByAge_recursive(name_node->right, surname_node->right, address_node->right, age_node->right,age);
}

Persona* findByName_recursive(IndexNodeString *name_node, IndexNodeString *surname_node, IndexNodeString *address_node, IndexNodeInt *age_node, char* name){
	if(name_node == NULL ) {
		return NULL;
	}
	if(strcmp(name_node->value,name)==0){
		Persona *out = createPersona(name_node->value,surname_node->value,address_node->value,age_node->value);
		printf("%s %s %s %d\n",name_node->value, surname_node->value, address_node->value, age_node->value);
		return out;
	}
	findByName_recursive(name_node->left, surname_node->left, address_node->left, age_node->left,name);
	findByName_recursive(name_node->right, surname_node->right, address_node->right, age_node->right,name);
}

Persona* findByName(Database * database, char * name){
	if(database == NULL || database->name == NULL) {
		return NULL;
	}
	findByName_recursive(database->name, database->surname, database->address, database->age, name);

}

Persona* findBySurname(Database * database, char * surname){
	if(database == NULL || database->surname == NULL) {
		return NULL;
	}
	findBySurname_recursive(database->name, database->surname, database->address, database->age, surname);
}
Persona* findByAddress(Database * database, char * address){
	if(database == NULL || database->address == NULL) {
		return NULL;
	}
	findByAddress_recursive(database->name, database->surname, database->address, database->age, address);
}
Persona* findByAge(Database * database, int age){
	if(database == NULL || database->age == NULL) {
		return NULL;
	}
	findByAge_recursive(database->name, database->surname, database->address, database->age, age);
}

void insert(Database * database, Persona * persona){
	if(database->name==NULL){
		database->name = createNodeString(persona->name);
		database->surname = createNodeString(persona->surname);
		database->address = createNodeString(persona->address);
		database->age = createNodeInt(persona->age);
	}else{
		insertString(database->name,persona->name);
		insertString(database->surname,persona->surname);
		insertString(database->address,persona->address);
		insertInt(database->age,persona->age);
	}
	return;
}

void insertString(IndexNodeString *node, char* value){
	if(node==NULL){
		return ;
	}
	if(node->left==NULL){
		IndexNodeString* newNode = createNodeString(value);
		node->left = newNode;	
		return;
	}else if(node->right==NULL){
		IndexNodeString* newNode = createNodeString(value);
		node->right = newNode;
		return;
	}
	insertString(node->left,value);
	insertString(node->right,value);
}

void insertInt(IndexNodeInt *node, int value){
	if(node==NULL){
		return;	
	}
	if(value<=node->value){
		if(node->left==NULL){
			IndexNodeInt* newNode = createNodeInt(value);
			node->left = newNode;
			return;
		}
		insertInt(node->left,value);
		return;
	}
	if(node->right==NULL){
		IndexNodeInt* newNode = createNodeInt(value);
		node->right = newNode;
		return;
	}
	insertInt(node->right,value);
}

IndexNodeString* createNodeString(char* value){
	IndexNodeString *node = malloc(sizeof(IndexNodeString));
	if(node==NULL){
		return NULL;
	}
	node->value = value;
	node->left=NULL;
	node->right=NULL;
	return node;
}

IndexNodeInt* createNodeInt(int value){
	IndexNodeInt *node = malloc(sizeof(IndexNodeInt));
	if(node==NULL){
		return NULL;
	}
	node->value = value;
	node->left=NULL;
	node->right=NULL;
	return node;
}

Persona* createPersona(char* name, char* surname, char* address, int age){
	Persona *p = malloc(sizeof(Persona));
	strcpy(p->name,name);
	strcpy(p->surname,surname);
	strcpy(p->address,address);
	p->age = age;
        return p;
}

